 <footer class="site-footer">

    <div class="site-footer__inner container container--narrow">

      <div class="group">

        <div class="site-footer__col-one">
		<a href="https://richmonddiocese.org/"><img src="<?php echo get_theme_file_uri('/images/200.jpg'); ?>"></a>
          <h1 class="school-logo-text school-logo-text--alt-color"><a href="<?php echo site_url(); ?>">St. Joan Of Arc</a></h1>
          <p><a class="site-footer__link" href="#">	315 Harris Grove Lane
													Yorktown Virginia 23692
													Parish Office:  757-898-5570</a></p>
        </div>

       

        <div class="site-footer__col-four">
		
          <h3 class="headline headline--small">Connect With Us</h3>
          <nav>
            <ul class="min-list social-icons-list group">
              <li><a href="#" class="social-color-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            </ul>
          </nav>
        </div>
      </div>

    </div>
  </footer>

	<?php wp_footer(); ?>
	</body>
</html>