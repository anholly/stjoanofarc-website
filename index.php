<?php get_header(); ?>
<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/divine_mercy_2016.jpg')?>);"></div>
    <div class="page-banner__content container t-center c-white">
      <h1 class="headline headline--medium">Welcome to St. Joan of Arc!</h1>
      <h2 class="headline headline--small">Come and Join Us.</h2>

	  </div>
	</div>
</div>
<!--  <hr>
  <h2 class="t-center">Parish News</h2>
<div class="tcontainer">
		<div class="ticker-wrap">
			<div class="ticker-move">
				<div class="ticker-item">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
				<div class="ticker-item">Aliquam consequat varius consequat.</div>
				<div class="ticker-item">Fusce dapibus turpis vb bel nisi malesuada sollicitudin.</div>
				<div class="ticker-item">Pellentesque auctor molestie orci ut blandit.</div>
			</div>
		</div>
		</div>
 <hr>
-->
  <div class="full-width-split group">
    <div class="full-width-split__one">
      <div class="full-width-split__inner">
        <h2 class="headline headline--small-plus t-center">Parish News</h2>
		
 <div class="tickery-wrap">
    <ul>
      <li>
		<?php $homepageEvents = new WP_Query(array(
				'posts_per_page' => -1,
				'post_type' => 'news',
				'meta_key' => 'event_date',
				'orderby' => 'meta_value_num',
				'order' => 'asc',
				'meta_query' => array(
					array(
						'key' => 'event_date ',
						'compare' => '>=',
						'value' => $today,
						'type' => 'numeric'
					)
				)
			));
			
			while($homepageEvents->have_posts()) {
				$homepageEvents->the_post(); ?>
				
	  
	  
	  </li>
      <li>Aliquam consequat varius consequat.</li>
      <li>Fusce dapibus turpis vel nisi malesuada sollicitudin.</li>
      <li>Pellentesque auctor molestie orci ut blandit.</li>
    </ul>
  </div>
 
 <!-- <div>
            <h3>Saturday</h3>
			<li>5:30pm</li>
			<hr>
			<h3>Sunday</h3>
			<li>8:30am & 11:30am</li>
        </div>
		<hr>
		<h3>Sacrament of Reconciliation</h3>
		<h4>Saturday</h4>
			<li>4:30pm</li>
		<div>

		</div>
         -->               
	</div>
    </div>
    <div class="full-width-split__two">
      <div class="full-width-split__inner">
        <h2 class="headline headline--small-plus t-center">Parish Staff</h2>
		
		<img src=<?php echo get_theme_file_uri('/images/Joly1.jpg'); ?> >
			<h5><a href="#">Father Micheal Joly</a></h5>
			

	<!-- <h3>Ministry of Healing</h3>
		<p>Please let the Parish Office know if you or a family member is sick, in
           the hospital, or suffering. We have resources available for you. We
           would like to help arrange for Communion visits, phone calls, meals,
           etc. Please know your parish cares and wants to help. Please contact
           Deacon Jim Satterwhite or Cherry Macababbad in the Parish Office
           at (757) 898-5570. Your call is confidential; we will only put names for
           prayers in the bulletin if you specifically request it. Please be sure to call when it
            is time to remove a name from the prayer list. Thank you. </p>

          -->
		 
  
     </div>
   
	</div>
        </div>

 <!-- <div class="hero-slider">
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/clouds.jpg')?>);">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Special Event or Celebration 1</h2>
        <p class="t-center"></p>
        <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
      </div>
    </div>
  </div>
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/divine_mercy_2016.jpg')?>);">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Special Event or Celebration 2</h2>
        <p class="t-center">Our dentistry program recommends eating apples.</p>
        <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
      </div>
    </div>
  </div>
  <div class="hero-slider__slide" style="background-image: url(<?php echo get_theme_file_uri('/images/stj3.jpg')?>);">
    <div class="hero-slider__interior container">
      <div class="hero-slider__overlay">
        <h2 class="headline headline--medium t-center">Special Event or Celebration 3</h2>
        <p class="t-center">Fictional University offers lunch plans for those in need.</p>
        <p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
      </div>
    </div>
  </div>
</div>
-->

<?php get_footer();
?>
