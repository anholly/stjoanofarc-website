<?php get_header(); ?>
<div class="page-banner">
  <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/sja2.jpg')?>);"></div>
    <div class="page-banner__content container t-center c-white ">
      <h1 class="headline headline--medium">Welcome to St. Joan of Arc!</h1>
      <h2 class="headline headline--small">Come and Join Us.</h2>

	  </div>
	</div>
</div>
<div>
	<h3 class="t-center"><a href="<?php echo site_url('/bulletin'); ?>">Bulletin</a> / <a href="#mass-schedule">Mass Schedule</a></h3>



</div>
<hr>

  <div class="full-width-split group">
    <div class="full-width-split__one ray">
      <div class="full-width-split__inner">
	  
	<h2 class="headline headline--small-plus t-center">A Message From Your Pastor</h2>
		
		<img class="center" src=<?php echo get_theme_file_uri('/images/Joly1.jpg'); ?> >
			<h4 class="t-center"><a href="#">Father Micheal Joly</a></h4>
		<div>
			<?php
				$post_id = 42;
				$queried_post = get_post($post_id);
				echo $queried_post->post_content;
				?>
		</div>
     	   
<hr>
 
	</div>
    </div>
    <div class="full-width-split__two">
      <div class="full-width-split__inner">
        <h2 class="headline headline--small-plus t-center">Parish News</h2>
		
 <div > <!--class="tickery-wrap" -->
  <!-- <marquee class="tickery-wrap"  behavior="scroll" direction="down" onmouseover="this.stop();" onmouseout="this.start(); " scrollamount="11" loop=""> -->
      <div>
			<?php 
			
			$homepageEvents = new WP_Query(array(
				'post_type' => 'news',
				
			));
			
			while($homepageEvents->have_posts()) {
			$homepageEvents->the_post(); } ?>
				
			<?php the_content(); ?>
		
	  </div>
  </marquee> 
  </div>
  
     </div>
   

</div>
 </div>
<hr>
<br>
<div  id="mass-schedule">
<?php
$post_id = 1;
$queried_post = get_post($post_id);
$content = $queried_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
echo $content;
?>
<?php
$post_id = 67;
$queried_post = get_post($post_id);
$content = $queried_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
echo $content;
?>
</div>

<?php get_footer();
?>
