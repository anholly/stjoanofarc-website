<!DOCTYPE html>
<html>
  <head>
  <meta name="viewport" content="width=device-width, intial-scale=1">
  
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="site-header freeze">
    <div class="container">
      <h1 class="school-logo-text float-left"><a href="<?php echo site_url(); ?>">St.Joan of Arc Catholic Church</a></h1>
     <!-- <span class="js-search-trigger site-header__search-trigger"><i class="fa fa-search" aria-hidden="true"></i></span> -->
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="main-navigation">
          <ul>
            <li><a href="<?php echo site_url('/about-us'); ?>">About Us</a></li>
			<li><a>Join Us</a></li>
			<li class="dropdown"><a class="">Ministries</a>
				<div class="dropdown-content">
					<a href="#">Adult Faith Formation</a>
					<a href="#">Bible Study</a>
					<a href="#">Knights of Columbus</a>
					<a href="#">Litugical</a>
					<a href="#">Non-Litugical</a>
					<a href="#">Parish Pastoral Council</a>
					<a href="#">Young Adults</a>
					<a href="#">RCIA</a>
				</div>
			</li>
			<li class="dropdown"><a>Religious Education</a>
				<div class="dropdown-content">
					<a href="#">Catechist</a>
					<a href="<?php echo site_url('/faith-formation'); ?>">Faith Foramtion</a>
					<a href="#">Middle School Ministry</a>
					<a href="#">High School Ministry</a>
					<a href="#">Question Of The Month</a>
				</div>
			</li>
			<li class="dropdown"><a>Media</a>
				<div class="dropdown-content">
					<a href="#">Photos</a>
					<a href="#">Videos</a>
				</div>
			</li>
			<li><a>Articles</a></li>
            <li class="dropdown"><a>Sacraments</a>
				<div class="dropdown-content">
					<a href="<?php echo site_url('/baptism'); ?>">Baptism</a>
					<a href="<?php echo site_url('/First Communion'); ?>">First Communion</a>
					<a href="<?php echo site_url('/Reconciliation'); ?>">Reconciliation</a>
					<a href="<?php echo site_url('/Confirmation'); ?>">Confirmation</a>
					<a href="<?php echo site_url('/Marriage'); ?>">Marriage</a>
				</div>
			</li>
            <li class="dropdown"><a href="#">Events</a>
				<div class="dropdown-content">
					<a href="#">Upcoming Events</a>
					<a href="#">Past Events</a>
				</div>
			
			</li>
            <li><a href="<?php echo site_url('/contact-us'); ?>">Contact Us</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
