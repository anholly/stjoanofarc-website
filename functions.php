<?php 

function university_files(){
	wp_enqueue_script('main-uiversity-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
	wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'); 
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'); 
	wp_enqueue_style('font-awesome', '//www.w3schools.com/w3css/4/w3.css'); 
	wp_enqueue_style('university_main_styles', get_stylesheet_uri(), NULL, microtime());
}

add_action('wp_enqueue_scripts','university_files');

function university_features() {
	add_theme_support('title-tag');
	add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'university_features');

function university_adjust_queries($query) {
	if (!is_admin() AND is_post_type_archive('news') AND $query->is_main_query()) {
		$query->set('orderby', 'title');
		$query->set('order','ASC');
		$query->set('posts_per_page', -1 );
		
		
	}
	if (!is_admin() AND is_post_type_archive('news') AND $query->is_main_query()) {
		 $query->set('orderby', 'title');
		 $query->set('posts_per_page', -1 );
	}
	
}

add_action('pre_get_posts', 'university_adjust_queries');
add_filter( 'tablepress_edit_link_below_table', '__return_false' );
